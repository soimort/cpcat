#!/bin/sh
VERSION=0.0.1

function synchronize() {
    NAME=${S##*/}
    for T do
        T=${T%/}
        if [ -r $S -a -n "$T" ]; then
            echo "cpcat: syncing from $S to $T..."
            if [ -e "$T/$NAME" ]; then
                SBID=$RANDOM
                while [ -e "$T/$NAME~$SBID" ]; do
                    SBID=$RANDOM
                done
                mv $IS_VERBOSE -f $T/$NAME $T/$NAME~$SBID
                cp $IS_VERBOSE -fr $S $T/$NAME
                if [ $? -eq 0 ]; then
                    rm $IS_VERBOSE -fr $T/$NAME~$SBID
                else
                    echo "cpcat: sync from $S to $T failed"
                    exit 1
                fi
            else
                mkdir $IS_VERBOSE -p $T
                cp $IS_VERBOSE -fr $S $T/$NAME
                if [ ! $? -eq 0 ]; then
                    echo "cpcat: sync from $S to $T failed"
                    exit 1
                fi
            fi
        else
            echo "cpcat: sync from $S to $T failed"
            exit 1
        fi
    done
    exit 0
}

function archive() {
    NAME=${S##*/}
    for T do
        T=${T%/}
        if [ -r $S -a -n "$T" ]; then
            echo "cpcat: archiving from $S to $T..."
            TID=`date +%Y%m%d%H%M%S`
            tar $IS_VERBOSE -cJf $T/$NAME~$TID.tar.xz $S
            if [ ! $? -eq 0 ]; then
                echo "cpcat: archiving from $S to $T failed"
                exit 1
            fi
        fi
    done
    exit 0
}

while getopts "Vvs:a:" ARG
do
    case $ARG in
    V)
        echo "cpcat version: $VERSION"
        ;;
    v)
        IS_VERBOSE='-v'
        ;;
    s)
        S=${OPTARG%/}
        shift 2
        synchronize $*
        ;;
    a)
        S=${OPTARG%/}
        shift 2
        archive $*
        ;;
    ?)
        exit 1
        ;;
    esac
done
